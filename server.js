const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const departmentApi = require('./Server/Routes/departmentApi');
const userApi = require('./Server/routes/userApi');
const departmentRequestApi = require('./Server/routes/department_requestApi');
const gateRequestApi = require('./Server/routes/gate_requestApi');
const visistorRequest = require('./Server/routes/visitor_requestApi');
const port = 5000;

const app = express();
app.use(bodyParser.json());
app.use('/api/department',departmentApi);
app.use('/api/user',userApi);
app.use('/api/departmentRequest',departmentRequestApi);
app.use('/api/gateRequest',gateRequestApi);
app.use('/api/visitors',visistorRequest);
const connectionString = "mongodb://localhost:27017/gatepass";

mongoose.connect(connectionString,(err)=>{
    if(err)
    {
        console.log(err);
    }
});


app.listen(port,()=>{
    console.log(`Server stated at PORT ${port}`);
})