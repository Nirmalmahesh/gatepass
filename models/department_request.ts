import { UserDetails } from './userDetails';
import { AuthService } from 'src/app/auth.service';

export class Request{
    user_id : Number;
    visitorName : String;    
    purpose : Number;
    date : any;
    status : Number;
    gateNo : Number;
    approvalStatus : Number;
    numberOfVisitors : Number;
    enterDateTime : Date
    constructor(){ }
}