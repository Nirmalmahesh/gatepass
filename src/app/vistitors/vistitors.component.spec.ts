import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistitorsComponent } from './vistitors.component';

describe('VistitorsComponent', () => {
  let component: VistitorsComponent;
  let fixture: ComponentFixture<VistitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
