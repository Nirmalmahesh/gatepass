import { Component, OnInit } from '@angular/core';
import { BasicCrudService } from '../basic-crud.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-vistitors',
  templateUrl: './vistitors.component.html',
  styleUrls: ['./vistitors.component.css']
})
export class VistitorsComponent implements OnInit {

  private departments: any = [];
  private visitors: any = [];
  private department_id;
  constructor(private basicCrud: BasicCrudService,private _auth : AuthService) {

  }
  markExit  = async(request) =>{
    request.exitGate =await  this._auth.getFullUser().user_id;
    request.exitDateTime = new Date().toString();
    this.basicCrud.updateRequestGivenForDepartment(request).subscribe(data=>{
      console.log(data);
    })
  }
   parseISOString = (s)=> {
    var b = s.split(/\D+/);
    return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
  }
  handelChange = () => {
    this.getVisitors();
  }
  getAllDepartments = () => {
    this.basicCrud.getAllDepartments().subscribe(data => {
      this.departments = data;
    })
  }
  getVisitors() {
    console.log(this.department_id);
    this.basicCrud.getAllAprovedVisitors(this.department_id).subscribe(data => {      
      this.visitors = data;
      this.visitors = this.visitors.map(element =>{
        this.basicCrud.getUserById(element.accepterId).subscribe(
          async data => {
            element["accepter"] = data;
          }
        )
        return element;
      })
      
    })
  }

  ngOnInit() {
    this.getAllDepartments();
    this.getVisitors();
  }
  getUserById =  (userId) => {
    let user = null;
     this.basicCrud.getUserById(userId).subscribe(
      async data => {
        user = await data;
      }
    )
    console.log(user);
    
  }

}
