import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Request } from 'models/department_request';
import { BasicCrudService } from '../basic-crud.service';
import { CustomError } from 'models/error';
import { async } from '@angular/core/testing';
import { user } from 'models/user';

@Component({
  selector: 'app-give-request',
  templateUrl: './give-request.component.html',
  styleUrls: ['./give-request.component.css']
})
export class GiveRequestComponent implements OnInit {

  request = new Request();
  customError = new CustomError();
  showError = false;

  hideError = () => {
    this.showError = !this.showError;
  }
  constructor(private _auth: AuthService, private basicCrud: BasicCrudService) { }
  giveRequest = () => {
    let userDetails = {
      "user_id": this._auth.getFullUser().user_id
    };
    this.request["userDetails"] = userDetails;
    
    this.basicCrud.giveRequests(this.request).subscribe(
      data => {
        this.customError.errorMessage = "Request Sended Successfully";
        this.customError.type = "success";
        this.showError = true;
        console.log(this.customError);
      }
    );
  }
  ngOnInit() {

  }

}
