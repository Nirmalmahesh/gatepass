import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiveRequestComponent } from './give-request.component';

describe('GiveRequestComponent', () => {
  let component: GiveRequestComponent;
  let fixture: ComponentFixture<GiveRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiveRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiveRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
