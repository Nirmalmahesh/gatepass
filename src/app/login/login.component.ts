import { Component, OnInit } from '@angular/core';
import { user } from 'models/user';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

 user = new user('','');

 submitUser = () =>{  
  this._auth.ValidateUser(this.user).subscribe(
    (data)=>{
      localStorage.setItem("USER",JSON.stringify(data));
      this._auth.navigate();
    },
    (err)=>{
      console.log(err);
    }
  )

 }
  constructor(private _auth : AuthService,private router : Router) { }

  ngOnInit() {
    this._auth.navigate();
  }

}
