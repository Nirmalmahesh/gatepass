import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DepartmentDashboardComponent } from './department-dashboard/department-dashboard.component';
import { GiveRequestComponent } from './give-request/give-request.component';
import { SendedRequestDepartmentComponent } from './sended-request-department/sended-request-department.component';
import { GateDashboardComponent } from './gate-dashboard/gate-dashboard.component';
import { ViewRequestsGateComponent } from './view-requests-gate/view-requests-gate.component';
import { GateSendRequestComponent } from './gate-send-request/gate-send-request.component';
import { SendedRequestGateComponent } from './sended-request-gate/sended-request-gate.component';
import { VistitorsComponent } from './vistitors/vistitors.component';

const routes: Routes = [
  {path : '',redirectTo:'login',pathMatch:'full'},
  {path:'login',component:LoginComponent},
  {
    path:'department',component:DepartmentDashboardComponent,
    children : [
      {path : 'giveRequest',component:GiveRequestComponent},
      {path : 'viewRequest',component:SendedRequestGateComponent},
      {path : 'sended',component:SendedRequestDepartmentComponent}
    ]
  },
  {
    path : 'gate',component:GateDashboardComponent,
    children : [
      {path : 'viewRequests',component:ViewRequestsGateComponent},
      {path : 'sendRequest',component:GateSendRequestComponent},
      {path : 'visiters',component:VistitorsComponent}
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
