import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  navigate = () =>{
    console.log('check check');
    const user = JSON.parse(localStorage.getItem("USER"));
    if(user)
    {
      if(user.role.roleName === "DEPARTMENT")
      {
          this.router.navigate(['department']);
      }else if(user.role.roleName === "GATE")
      {
        this.router.navigate(["gate"])
      }
    }else{
      this.router.navigate(['login'])
    }
  }
  ValidateUser=(user)=>{
    return this.http.post('/api/user/login',user);
  }
  getUser = () =>{
    const user = JSON.parse(localStorage.getItem("USER"));
    return user.username;
  }
  getFullUser = ()=>{
    const user = JSON.parse(localStorage.getItem("USER"));
    return user;
  }
  getCatogery = () =>{
    const user = JSON.parse(localStorage.getItem("USER"));
    if(user)
    {
      return user.role.roleName;
    }else{
      return null;
    }
  }

  constructor(private http : HttpClient,private router : Router) { }
}
