import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private _auth : AuthService,private router : Router) { }
 
  logout = () =>{
    localStorage.removeItem("USER");
    this._auth.navigate();
  }
  isUser = () =>{    
    return this._auth.getCatogery();    
  }
  ngOnInit() {

    

  }

}
