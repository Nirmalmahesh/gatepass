import { Component, OnInit } from '@angular/core';
import { BasicCrudService } from '../basic-crud.service';
import { Request } from 'models/department_request';
import { CustomError } from 'models/error';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-gate-send-request',
  templateUrl: './gate-send-request.component.html',
  styleUrls: ['./gate-send-request.component.css']
})
export class GateSendRequestComponent implements OnInit {

  departments;
  customError = new CustomError();
  showError;
  selected_department_id;
  hideError = () => {
    this.showError = !this.showError;
  }
  constructor(private basicCrud: BasicCrudService, private _auth: AuthService) { }
  request = new Request();

  giveRequest = () => {

    this.request.status = -1;
    this.request.date = new Date();
    let userDetails = {
      "user_id": this._auth.getFullUser().user_id
    };
    let department = {
      "departmentId":this.selected_department_id
    }
    this.request["userDetails"] = userDetails;
    this.request["department"] = department;
    this.request["accepterId"] = this._auth.getFullUser().user_id;
    console.log(this.request);
    this.basicCrud.giveGateRequest(this.request).subscribe(
      data => {
        this.customError.errorMessage = "Request Sended Successfully";
        this.customError.type = "success";
        this.showError = true;
        console.log(this.customError);
      }
    );
  }
  getAllDepartment = () => {
    this.basicCrud.getAllDepartments().subscribe(
      data => {
        console.log(data);
        this.departments = data;
      }
    );
  }
  ngOnInit() {
    this.getAllDepartment();
  }

}
