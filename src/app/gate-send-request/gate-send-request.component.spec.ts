import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GateSendRequestComponent } from './gate-send-request.component';

describe('GateSendRequestComponent', () => {
  let component: GateSendRequestComponent;
  let fixture: ComponentFixture<GateSendRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GateSendRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GateSendRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
