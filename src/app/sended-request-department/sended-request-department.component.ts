import { Component, OnInit } from '@angular/core';
import { BasicCrudService } from '../basic-crud.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sended-request-department',
  templateUrl: './sended-request-department.component.html',
  styleUrls: ['./sended-request-department.component.css']
})
export class SendedRequestDepartmentComponent implements OnInit {
  requests ;
  constructor(private basicCrud : BasicCrudService,private _auth : AuthService) { }
loadRequests = async() =>{  
  
  await this.basicCrud.getAllRequestByDepartmentToGate( this._auth.getFullUser().user_id).subscribe(
    data=>{      
      this.requests = data;
    }
  )
}
  ngOnInit() {
    this.loadRequests();
  }

}
