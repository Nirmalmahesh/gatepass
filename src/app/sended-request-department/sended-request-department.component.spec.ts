import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendedRequestDepartmentComponent } from './sended-request-department.component';

describe('SendedRequestDepartmentComponent', () => {
  let component: SendedRequestDepartmentComponent;
  let fixture: ComponentFixture<SendedRequestDepartmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendedRequestDepartmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendedRequestDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
