import { Component, OnInit } from '@angular/core';
import { BasicCrudService } from '../basic-crud.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sended-request-gate',
  templateUrl: './sended-request-gate.component.html',
  styleUrls: ['./sended-request-gate.component.css']
})
export class SendedRequestGateComponent implements OnInit {

  constructor(private basicCrud: BasicCrudService, private _auth: AuthService) { }
  requests;
  approve = (request) => {
    request.approvalStatus = 1;
    //request.accepterId = this._auth.getFullUser().user_id;
    this.basicCrud.updateRequestGivenForDepartment(request).subscribe(
      data => {
        console.log(data);
        this.loadRequests();
      }
    )
  }
  reject = (request) => {
    request.approvalStatus = -1;
    request.rejectorId = this._auth.getFullUser().user_id;
    this.basicCrud.updateRequestGivenForDepartment(request).subscribe(
      data => {
        console.log(data);
        this.loadRequests();
      }
    )
  }
  loadRequests = async () => {
    await this.basicCrud.getAllRequestOfDepartmenByGate(this._auth.getFullUser().department.departmentId).subscribe(
      data => {
        this.requests = data;
        console.log(data);
      }
    )
  }

  ngOnInit() {
    this.loadRequests();
  }

}
