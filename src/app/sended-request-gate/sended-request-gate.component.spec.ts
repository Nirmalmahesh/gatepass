import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendedRequestGateComponent } from './sended-request-gate.component';

describe('SendedRequestGateComponent', () => {
  let component: SendedRequestGateComponent;
  let fixture: ComponentFixture<SendedRequestGateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendedRequestGateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendedRequestGateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
