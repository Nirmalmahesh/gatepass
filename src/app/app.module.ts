import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import {HttpClientModule} from '@angular/common/http';
import { DepartmentDashboardComponent } from './department-dashboard/department-dashboard.component';
import { GiveRequestComponent } from './give-request/give-request.component';
import { SendedRequestDepartmentComponent } from './sended-request-department/sended-request-department.component';
import { ViewRequestsGateComponent } from './view-requests-gate/view-requests-gate.component';
import { GateDashboardComponent } from './gate-dashboard/gate-dashboard.component';
import { GateSendRequestComponent } from './gate-send-request/gate-send-request.component';
import { SendedRequestGateComponent } from './sended-request-gate/sended-request-gate.component';
import { VistitorsComponent } from './vistitors/vistitors.component'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    DepartmentDashboardComponent,
    GiveRequestComponent,
    SendedRequestDepartmentComponent,
    ViewRequestsGateComponent,
    GateDashboardComponent,
    GateSendRequestComponent,
    SendedRequestGateComponent,
    VistitorsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
