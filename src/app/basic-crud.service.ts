import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class BasicCrudService {

  getAllDepartments = () =>{
    return this._http.get('/api/department/getAll')
  }
  giveRequests = (request) =>{
    return this._http.post('/api/request/new',request);
  }
  updateRequest = (data)=>{
    return this._http.post('api/departmentRequest/update',data);
  }
  getAllRequestByDepartment =  (data) =>{
    console.log(data);
    return this._http.get(`/api/departmentRequest/${data}`);
  }
  giveGateRequest = (data) =>{
    return this._http.post('/api/gateRequest/giveRequest',data);
  }
  getAllRequestByDepartmentToGate = (data) =>{      
    return this._http.get(`/api/findRequest/${data}`);
  }
  getAllRequestOfDepartmenByGate = (department_id) =>{
    console.log(department_id);
    return this._http.get(`/api/findRequest/gate/${department_id}`);
  }
  updateRequestGivenForDepartment = (data) =>{
    return this._http.put('/api/gateRequest/update',data);
  }
  getAllAprovedVisitors = (data) =>{
    return this._http.get(`/api/get/Approved/${data}`);
  }
  getUserById = (userId) =>{
      return this._http.get(`api/user/${userId}`);
  } 
  constructor(private _http : HttpClient) { }
}
