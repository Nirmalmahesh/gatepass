import { Component, OnInit } from '@angular/core';
import { BasicCrudService } from '../basic-crud.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-view-requests-gate',
  templateUrl: './view-requests-gate.component.html',
  styleUrls: ['./view-requests-gate.component.css']
})
export class ViewRequestsGateComponent implements OnInit {
  requests;
  departments;
  selected_department_id;
  approve = async (request)=>{
    request.approvalStatus = 1;
    request.accepterId = this._auth.getFullUser().user_id;
    await this.basicCrud.updateRequestGivenForDepartment(request).subscribe(data=>{console.log(data)});
    await this.loadRequests();

  }
  reject = async (request)=>{
    request.approvalStatus = -1;
    request.rejectorId = this._auth.getFullUser().user_id;
    await this.basicCrud.updateRequestGivenForDepartment(request).subscribe(data=>{console.log(data)});
    await this.loadRequests();

  }
  loadDepartments = ()=>{this.basicCrud.getAllDepartments().subscribe(
    data=>{
      this.departments = data;
      console.log(data);
    }
  );

  }
  loadRequests = async() =>{  
    
    await this.basicCrud.getAllRequestByDepartment( this.selected_department_id).subscribe(
  
      data=>{      
        this.requests = data;
        console.log(data);
      }
    )
  }
  constructor(private basicCrud : BasicCrudService,private _auth : AuthService) { }

  ngOnInit() {
    this.loadRequests();
    this.loadDepartments();
  }

}
