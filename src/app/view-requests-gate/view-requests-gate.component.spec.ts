import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRequestsGateComponent } from './view-requests-gate.component';

describe('ViewRequestsGateComponent', () => {
  let component: ViewRequestsGateComponent;
  let fixture: ComponentFixture<ViewRequestsGateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRequestsGateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRequestsGateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
