const mongoose = require('mongoose');

const departmentRequest = new mongoose.Schema({
    department_id : Number,
    leader_name : String,
    number_of_persons : Number,
    reason : String,
    
    status : Number
});

module.exports = mongoose.model('department_request',departmentRequest,'department_requests');