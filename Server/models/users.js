const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username : String,
    password : String,
    user_id : Number,
    department_id : Number,
    catogery : String
});

module.exports = mongoose.model('user',userSchema,'users');