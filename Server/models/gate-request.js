const mongoose = require('mongoose');

const gateRequest = new mongoose.Schema({
    department_id : Number,
    leader_name : String,
    number_of_persons : Number,
    reason : String,
    date : String,
    status : Number,
    gate_no : Number
});

module.exports = mongoose.model('gate_request',gateRequest,'gate_requests');