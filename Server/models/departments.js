const mongoose = require('mongoose');

const departmentSchema = new mongoose.Schema({
    department_id : Number,
    name : String,
    short_name : String
});

module.exports = mongoose.model('department',departmentSchema,'departments');