const express = require('express');
const GateRequest = require('../models/gate-request');
const DepartmentRequest = require('../models/department-request');
const router = express.Router();

router.post('/getAllApproved',(req,res)=>{
    let data = req.body;
    GateRequest.find({department_id : data.department_id,status: 1}).exec((err,document)=>{        
        if(err)
            console.log(err);
        else
            {
                let gateRequests = document;
                DepartmentRequest.find({department_id : data.department_id,status : 1}).exec((err,document)=>{
                    if(err){
                        console.log(err);
                    }else{
                        let departmentRequest = document;
                       // res.json(gateRequests.concat(departmentRequest));
                       res.json(gateRequests);
                    }
                })
            }
    })
})

module.exports = router;