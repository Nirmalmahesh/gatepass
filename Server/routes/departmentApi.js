const express = require('express');
const Department = require('../models/departments');
const router = express.Router();


router.get('/getAllDepartments',(req,res)=>{    
    Department.find({}).exec((err,document)=>{
        if(err)
        {
            console.log(err);
        }else{            
            res.setHeader('Content-Type','Application/Json');
            res.json(document);            
        }
    })
});


module.exports = router;

