const express = require('express');
const Request = require('../models/gate-request');
const router = express.Router();

router.post('/giveRequest',(req,res)=>{    
    var request = new Request(req.body);
    request.save((err,insertedDocument)=>{
        if(err)
        {
          console.log(err);
        }else
        {
            res.json(insertedDocument);
        }
    })   
});
router.post('/findRequests',(req,res)=>{
    Request.find(req.body).exec((err,document)=>{
        if(err)
            console.log(err);
        else
            res.json(document);
    })
})
router.post('/update',(req,res)=>{
    console.log(req.body);
    Request.findOneAndUpdate(req.body[0],{$set : req.body[1]},{new:true,upsert: true},(err,updatedDocument)=>{
        if(err)
        {
            console.log(err);
        }else{
            res.json(updatedDocument);
        }
    });
})


module.exports = router;