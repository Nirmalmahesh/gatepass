const express = require('express');
const User = require('../models/users');
const router = express.Router();

router.post('/getUser',async (req,res)=>{       
    await User.find(req.body).exec(async (err,document)=>{
        if(err)
        {
            console.log(err);
        }else{                       
            res.setHeader('Content-Type','Application/Json');             
            if(document.length===1)
            {
                res.statusCode = 200;                
                await res.json({catogery:document[0].catogery,username:document[0].username,department_id:document[0].department_id});
            }else{
                res.statusCode = 500;
                res.statusMessage = "User Not Found";
                res.json({error : "User Not Found"})
            }          
        }
    })
});


module.exports = router;

